var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index');
});

router.get('/about', (req, res) => {
  res.render('about');
});

router.get('/contact', (req, res) => {
  res.render('contact');
});

router.post('/contact', (req, res) => {
  res.end();
});

router.get('/why-non-woven-bags', (req, res) => {
  res.render('non-woven');
})

router.get('/infrastructure', (req, res) => {
  res.render('infrastructure');
})

module.exports = router;
